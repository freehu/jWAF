## jWAF
JAVA版本的开源WAF（Java Web Application Firewall）
## Why
> 任何外置的WAF都存在被绕过的可能，应用系统不应放弃自身的安全防护。

## Advantage
以jar包的方式集成到WEB应用中，即使攻击者绕过了外置的WAF，由于自身具备安全防护能力，能有效保护信息系统的安全
## Features
* 攻击防护
	* SQL注入防护
	* XSS攻击防护
	* 点击劫持防护
* 输入校验
	* 敏感字符替换
* 限额管理
	* 最大会话数限制
	* 同一账号登录数限制

* 异常管理
	* 记录500异常
	* 异常URL防护

## How To
请参考 [使用手册](https://gitee.com/haison/jWAF/blob/master/docs/Guide.md)